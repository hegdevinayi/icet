.. index::
   single: Monte Carlo; Ensembles

.. module:: mchammer.ensembles

.. _ensembles:

Ensembles
=========


.. _canonical_ensemble:

.. index::
   single: Class reference; CanonicalEnsemble
   single: Monte Carlo; Canonical ensemble

Canonical ensemble
------------------

.. autoclass:: CanonicalEnsemble
   :members:
   :undoc-members:
   :inherited-members:



.. _sgc_ensemble:

.. index::
   single: Class reference; SemiGrandCanonicalEnsemble
   single: Monte Carlo; Semi-grand canonical ensemble

Semi-grand canonical ensemble
-----------------------------

.. autoclass:: SemiGrandCanonicalEnsemble
   :members:
   :undoc-members:
   :inherited-members:



.. _vcsgc_ensemble:

.. index::
   single: Class reference; VCSGCEnsemble
   single: Monte Carlo; Variance-constrained semi-grand canonical ensemble

Variance-constrained semi-grand canonical ensemble
--------------------------------------------------

.. autoclass:: VCSGCEnsemble
   :members:
   :undoc-members:
   :inherited-members:
