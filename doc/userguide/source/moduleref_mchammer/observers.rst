.. index::
   single: Monte Carlo; Observers

.. module:: mchammer.observers

Observers
=========

.. index::
   single: Class reference; ClusterExpansionObserver
   single: Monte Carlo; Cluster expansion observer

ClusterExpansionObserver
------------------------

.. autoclass:: ClusterExpansionObserver
   :members:
   :undoc-members:
   :inherited-members:

      
.. index::
   single: Class reference; SiteOccupancyObserver
   single: Monte Carlo; Site occupancy observer

SiteOccupancyObserver
---------------------

.. autoclass:: SiteOccupancyObserver
   :members:
   :undoc-members:
   :inherited-members:
