.. index::
   single: Function reference; Tools
   single: Class reference; Tools

Tools
=====

.. index::
   single: Function reference; Convex hull
   single: Class reference; Convex hull

Convex hull construction
------------------------

.. automodule:: icet.tools.convex_hull
   :members:
   :undoc-members:
   :inherited-members:

.. index::
   single: Function reference; Structure mapping

Mapping structures
------------------

.. automodule:: icet.tools.structure_mapping
   :members:
   :undoc-members:
   :inherited-members:

.. _structure_enumeration:

.. index::
   single: Function reference; Structure enumeration
   single: Class reference; Structure enumeration

Structure enumeration
---------------------

.. automodule:: icet.tools.structure_enumeration
   :members:
   :undoc-members:
   :inherited-members:
