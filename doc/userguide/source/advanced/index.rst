.. _tutorial_advanced_topics:
.. index:: Tutorial, advanced topics

Advanced topics
***************

This section demonstrates the use of :program:`icet` via a set of examples that
showcase different features and modes of usage.

The scripts and database that are required for this tutorial can be downloaded
as a single zip archive. To this end, run the following command from the
command line::

    curl -O https://icet.materialsmodeling.org/tutorial_<version>.zip
    unzip tutorial_<version>.zip

Here, ``<version>`` has to be replaced by the :program:`icet` version that is
installed.

.. toctree::
   :maxdepth: 1
   :caption: Contents

   structure_enumeration
   cluster_counts
   cluster_space_info
   cluster_vectors
   mapping_structures
   cluster_vector_correlations
   neighbor_list
   permutation_matrix
   parallel_monte_carlo
